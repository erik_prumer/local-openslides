import io
import os.path
import shutil
import subprocess
import sys
import tempfile
import webbrowser
from enum import Enum
from pathlib import Path

import clipboard
import pyperclip
import qrcode
import win32clipboard
from PIL import Image
from PySide6 import QtCore, QtGui
from qrcode.image.styledpil import StyledPilImage
from qrcode.image.styles.moduledrawers.pil import RoundedModuleDrawer
from qrcode.image.styles.colormasks import RadialGradiantColorMask

from PySide6.QtCore import QObject, QRunnable, Signal, Slot, QThreadPool, Qt
from PySide6.QtGui import QPixmap
from PySide6.QtWidgets import QMainWindow, QApplication, QSizeGrip

from ui.ui_MainWindow import Ui_MainWindow


class Pages(Enum):
    INSTALL = 2
    LOG = 0
    LOGIN = 3
    STARTEN = 1
    INSTALL_WSL = 4


def generate_qr_code(url: str):
    paths = []
    for box_size in [13, 40]:
        # Create a QR code instance
        print(box_size)
        qr = qrcode.QRCode(
            version=2,  # Adjust the version as needed
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=box_size,  # Adjust the box size as needed
            border=0  # Adjust the border size as needed
        )

        qr.add_data(url)
        qr.make(fit=True)

        color_mask = RadialGradiantColorMask(back_color=(255, 237, 0), center_color=(229, 0, 125),
                                             edge_color=(229, 0, 125))  # Change start and end colors as needed
        # Create an image with the QR code
        qr_img = qr.make_image(
            image_factory=StyledPilImage,
            module_drawer=RoundedModuleDrawer(),
            eye_drawer=RoundedModuleDrawer(),
            color_mask=color_mask
        )

        # Save the final QR code image
        with tempfile.NamedTemporaryFile(delete=False, suffix=".png") as temp_file:
            qr_img.save(temp_file.name)
            paths += [temp_file.name]

    return paths


class StartOSWorker(QObject, QRunnable):
    finished_starting = Signal(str, str, str, list)
    finished_stopping = Signal()
    save_proc = Signal(subprocess.Popen)

    def __init__(self, start_btn_checked: bool, username: str, password: str, os_proc: subprocess.Popen):
        QRunnable.__init__(self)
        QObject.__init__(self)

        self.start_btn_checked = start_btn_checked
        self.username = username
        self.password = password
        self.os_proc = os_proc

    @Slot()
    def run(self) -> None:
        if not self.start_btn_checked:
            self.os_proc.kill()
            subprocess.run(["wsl", "--distribution", "Ubuntu", "--user", self.username, "sh", "-c",
                            f"cd /etc/OpenSlides && echo {self.password} | sudo -S docker-compose down"],
                           capture_output=True,
                           text=True)
            self.finished_stopping.emit()
        else:
            password = subprocess.run(
                ["wsl", "--distribution", "Ubuntu", "--user", self.username, "sh", "-c",
                 f"curl ipv4.icanhazip.com"], capture_output=True, text=True).stdout
            self.os_proc = subprocess.Popen(["wsl", "--distribution", "Ubuntu", "--user", self.username, "sh", "-c",
                                             f"cd /etc/OpenSlides && echo {self.password} | sudo -S ./run.sh"],
                                            shell=True,
                                            stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            address = self.os_proc.stdout.readline().decode('utf-8', errors="ignore")[0:-1]
            self.save_proc.emit(self.os_proc)

            paths = generate_qr_code(address[13:])
            self.finished_starting.emit(address[13:], password[:-1], "Stopp", paths)


class InstallWorker(QObject, QRunnable):
    log = Signal(str)
    finished = Signal()

    def __init__(self, username: str, password: str):
        QRunnable.__init__(self)
        QObject.__init__(self)

        self.username = username
        self.password = password

    @Slot()
    def run(self) -> None:
        shutil.copy(f"{os.getcwd()}\\install_openslides.sh", "//wsl$/Ubuntu/tmp")
        subprocess.run(["wsl", "--distribution", "Ubuntu", "--user", self.username, "sh", "-c",
                        f"echo {self.password} | sudo -S chmod +x /tmp/install_openslides.sh"])
        proc = subprocess.Popen(["wsl", "--distribution", "Ubuntu", "--user", self.username, "sh", "-c",
                                 f"echo {self.password} | sudo -S /tmp/install_openslides.sh"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        line = ""
        while line != "Skript erfolgreich abgeschlossen":
            line = proc.stdout.readline().decode('utf-8', errors="ignore")[0:-1]
            if not line.strip():
                continue
            self.log.emit(line)

        self.finished.emit()


class LoginWorker(QObject, QRunnable):
    finished = Signal(int)
    set_userdata = Signal(str, str)

    def __init__(self, username: str, password: str):
        QRunnable.__init__(self)
        QObject.__init__(self)

        self.username = username
        self.password = password

    @Slot()
    def run(self) -> None:
        res = subprocess.run(["wsl", "--distribution", "Ubuntu", "--user", self.username, "sh", "-c",
                              f"echo {self.password} | sudo -S echo correct"], capture_output=True,
                             text=True)
        if "correct" in res.stdout:
            self.set_userdata.emit(self.username, self.password)

            res = subprocess.run(
                ["wsl", "--distribution", "Ubuntu", "--user", self.username, "sh", "-c",
                 f"ls /etc/OpenSlides"], capture_output=True, text=True)

            if (res.stdout == "docker-compose.yml\nopenslides\nrun.sh\nsecrets\n"):
                self.finished.emit(Pages.STARTEN.value)
            else:
                self.finished.emit(Pages.INSTALL.value)


class MainWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)  # Ruft Konstruktor der Super-Klasse auf
        self.ui = Ui_MainWindow()  # Erstellt die UI
        self.ui.setupUi(self)  # Initialisiert die UI

        self.setWindowFlags(Qt.FramelessWindowHint)  # Entferne den Rahmen des Fensters
        self.ui.minimize_button.clicked.connect(lambda: self.showMinimized())  # Minimiere das Fenster
        self.ui.close_button.clicked.connect(lambda: self.close())  # Schließe das Fenster

        self.ui.copy_qr_code_button.setVisible(False)

        def restore_or_maximize_window():
            """
            Prüft, ob das Fenster maximiert ist und toggled den Zustand. Passt das Icon des Maximieren-Button oben
            rechts entsprechend an.
            """

            if not self.ui.maximize_button.isChecked():
                self.showNormal()
            else:
                self.showMaximized()

        self.ui.maximize_button.clicked.connect(restore_or_maximize_window)  # Max- oder Minimiere das F.

        # MouseMoveEvent. Macht, dass das Fenster bewegt werden kann.
        def moveWindow(e):
            """
            Wird aufgerufen, wenn die Maus bewegt wird. Wird zum Bewegen des Fensters genutzt.

            :param e: Event, wenn die Maus bewegt wird
            """

            if self.isMaximized() == False:  # Bewegung nur möglich, wenn des Fenster nicht maximiert ist
                if e.buttons() == Qt.LeftButton:  # Ist die linke Maustaste gedrückt?
                    self.move(self.pos() + e.globalPosition().toPoint()  - self.clickPosition)  # Bewege das Fenster
                    self.clickPosition = e.globalPosition().toPoint()   # Passe die Klickposition an
                    e.accept()  # Akzeptiere das Event

        self.ui.header_frame.mouseMoveEvent = moveWindow  # Setze das neue MouseMoveEvent
        QSizeGrip(self.ui.size_grip)  # Stelle ein, dass das Fenster in der Größe verstellbar ist

        self.username = ""
        self.password = ""
        self.os_proc = None

        self.ui.button_login.clicked.connect(lambda: self.login())
        self.ui.button_starten.clicked.connect(lambda: self.start_os())
        self.ui.label_webadresse.clicked.connect(
            lambda: webbrowser.open(self.ui.label_webadresse.text(), new=0, autoraise=True))
        self.ui.button_installieren.clicked.connect(lambda: self.installieren())
        self.ui.button_ready.clicked.connect(lambda: self.ui.stackedWidget.setCurrentIndex(Pages.STARTEN.value))

        self.threads = QThreadPool()

        res = subprocess.run(["wsl", "--list", "--all"], capture_output=True, text=True)

        if "Ubuntu" in res.stdout.replace("\x00", ""):
            self.ui.stackedWidget.setCurrentIndex(Pages.LOGIN.value)
        else:
            self.ui.stackedWidget.setCurrentIndex(Pages.INSTALL_WSL.value)

    def mousePressEvent(self, event: QtGui.QMouseEvent) -> None:
        """
        Wird aufgerufen, wenn die Maus geklickt wurde.

        :param event: Event, wenn die Maus geklickt wurde
        """

        self.clickPosition = event.globalPosition().toPoint()   # Aktualisiert die Klick-Position der Maus

    def installieren(self):
        self.ui.stackedWidget.setCurrentIndex(Pages.LOG.value)
        worker = InstallWorker(self.username, self.password)
        QApplication.setOverrideCursor(Qt.BusyCursor)

        def log_fun(line: str):
            self.ui.protokoll_installieren.insertHtml(line + "<br>")
            self.ui.protokoll_installieren.verticalScrollBar().setValue(self.ui.protokoll_installieren.verticalScrollBar().maximum())
        worker.log.connect(log_fun)

        def finished_fun():
            self.ui.button_ready.setEnabled(True)
            QApplication.restoreOverrideCursor()

        worker.finished.connect(finished_fun)
        self.threads.start(worker)

    def login(self):
        worker = LoginWorker(self.ui.lineEdit_username.text(), self.ui.lineEdit_password.text())
        QApplication.setOverrideCursor(Qt.BusyCursor)

        def set_userdata_fun(username: str, password: str):
            self.username = username
            self.password = password
        worker.set_userdata.connect(set_userdata_fun)

        def finished_fun(next_page: int):
            self.ui.stackedWidget.setCurrentIndex(next_page)
            QApplication.restoreOverrideCursor()
        worker.finished.connect(finished_fun)

        self.threads.start(worker)

    def start_os(self):
        worker = StartOSWorker(self.ui.button_starten.isChecked(), self.username, self.password, self.os_proc)
        QApplication.setOverrideCursor(Qt.BusyCursor)

        def finished_starting_fun(web: str, pwd: str, btn: str, qr_paths: list):
            self.ui.label_webadresse.setText(web)
            self.ui.label_passwort.setText(pwd)
            self.ui.button_starten.setText(btn)
            pixmap = QPixmap(qr_paths[0])
            self.ui.qrcode_label.setPixmap(pixmap)
            self.ui.stackedWidget_run.setCurrentIndex(1)
            self.ui.copy_qr_code_button.setVisible(True)
            def copy_fun():
                image = Image.open(qr_paths[1])

                output = io.BytesIO()
                image.convert("RGB").save(output, "BMP")
                data = output.getvalue()[14:]
                output.close()

                win32clipboard.OpenClipboard()
                win32clipboard.EmptyClipboard()
                win32clipboard.SetClipboardData(win32clipboard.CF_DIB, data)
                win32clipboard.CloseClipboard()
            self.ui.copy_qr_code_button.clicked.connect(copy_fun)
            QApplication.restoreOverrideCursor()

        worker.finished_starting.connect(finished_starting_fun)

        def finished_stopping_fun():
            self.ui.stackedWidget_run.setCurrentIndex(0)
            self.ui.button_starten.setText("Start")
            self.ui.qrcode_label.clear()
            self.ui.copy_qr_code_button.setVisible(False)
            self.ui.copy_qr_code_button.clicked.disconnect()
            QApplication.restoreOverrideCursor()

        worker.finished_stopping.connect(finished_stopping_fun)

        def save_proc(proc: subprocess.Popen):
            self.os_proc = proc

        worker.save_proc.connect(save_proc)
        self.threads.start(worker)


if __name__ == '__main__':
    app = QApplication(sys.argv)  # Starte die App
    window = MainWindow()  # Erstelle ein Main-Window
    window.show()
    sys.exit(app.exec())
