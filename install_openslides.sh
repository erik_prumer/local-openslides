#!/bin/sh
echo '<p style="font-size:15pt;font-weight: bold;">Update apt-Repositories...</p>'
sudo apt-get update
echo '<p style="font-size:15pt;font-weight: bold;">Upgrade apt-Repositories...</p>'
sudo apt-get upgrade -y
echo '<p style="font-size:15pt;font-weight: bold;">Installiere benötigte Software-Packete...</p>'
sudo apt-get install -y libffi-dev libssl-dev python3-dev python3 python3-pip npm
sudo npm install -g localtunnel
curl -fsSL https://get.docker.com -o /tmp/get-docker.sh
sudo sh /tmp/get-docker.sh
sudo pip3 install docker-compose
echo '<p style="font-size:15pt;font-weight: bold;">Verwalte Berechtigungen</p>'
sudo usermod -aG docker openslides
sudo systemctl enable docker
echo '<p style="font-size:15pt;font-weight: bold;">Lade Openslides herunter...</p>'
mkdir /etc/OpenSlides
cd /etc/OpenSlides/
wget https://github.com/OpenSlides/openslides-manage-service/releases/download/latest/openslides
chmod +x openslides
echo '<p style="font-size:15pt;font-weight: bold;">Installiere Openslides...</p>'
sudo ./openslides setup .
sudo docker-compose pull
sudo docker-compose up --detach
sudo ./openslides initial-data
sudo docker-compose stop
echo '<p style="font-size:15pt;font-weight: bold;">Erstelle Start-Skript</p>'
echo '#!/bin/sh
ctrl_c() {
        docker-compose down
}

trap ctrl_c INT
docker-compose up --detach
lt --port 8000 --local-https --allow-invalid-cert' > run.sh
chmod +x run.sh
echo "Skript erfolgreich abgeschlossen"
