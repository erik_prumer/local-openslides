from PySide6.QtGui import QPainter, Qt, QTransform
from PySide6.QtWidgets import QLabel


class RotatedLabel(QLabel):
    def __init__(self, text):
        super().__init__(text)

    def paintEvent(self, event):
        # create a QPainter object
        painter = QPainter(self)
        painter.setPen(Qt.GlobalColor.white)

        # get the size of the text
        text_rect = painter.boundingRect(self.rect(), self.alignment(), self.text())

        # create a QTransform object
        transform = QTransform()

        # move the origin to the center of the text
        transform.translate(text_rect.center().x(), text_rect.center().y())

        # rotate the coordinate system by the angle
        transform.rotate(-7)

        # move the origin back to the top left corner
        transform.translate(-text_rect.center().x(), -text_rect.center().y())

        # apply the transformation to the painter
        painter.setTransform(transform)

        # draw the text using the transformed painter
        painter.drawText(self.rect(), self.alignment(), self.text())