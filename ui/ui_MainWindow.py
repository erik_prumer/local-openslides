# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'MainWindowxXvfWQ.ui'
##
## Created by: Qt User Interface Compiler version 6.4.3
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QFrame, QGridLayout, QHBoxLayout,
    QLabel, QLineEdit, QMainWindow, QPushButton,
    QSizePolicy, QSpacerItem, QStackedWidget, QTextEdit,
    QVBoxLayout, QWidget)

from rotatedlabel import RotatedLabel
import resources_rc

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1173, 751)
        MainWindow.setStyleSheet(u"QObject{\n"
"	background-color: rgb(255, 237, 0);\n"
"}")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.centralwidget.setStyleSheet(u"#centralwidget{\n"
"border-color: lightgray;\n"
"border-style: solid;\n"
"border-width: 2px;\n"
"}")
        self.horizontalLayout = QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.main_body = QFrame(self.centralwidget)
        self.main_body.setObjectName(u"main_body")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.main_body.sizePolicy().hasHeightForWidth())
        self.main_body.setSizePolicy(sizePolicy)
        self.main_body.setStyleSheet(u"")
        self.main_body.setFrameShape(QFrame.StyledPanel)
        self.main_body.setFrameShadow(QFrame.Raised)
        self.verticalLayout_5 = QVBoxLayout(self.main_body)
        self.verticalLayout_5.setSpacing(0)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.verticalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.header_frame = QFrame(self.main_body)
        self.header_frame.setObjectName(u"header_frame")
        self.header_frame.setStyleSheet(u"")
        self.header_frame.setFrameShape(QFrame.NoFrame)
        self.header_frame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_3 = QHBoxLayout(self.header_frame)
        self.horizontalLayout_3.setSpacing(0)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.slide_menu_button_frame = QFrame(self.header_frame)
        self.slide_menu_button_frame.setObjectName(u"slide_menu_button_frame")
        self.slide_menu_button_frame.setFrameShape(QFrame.StyledPanel)
        self.slide_menu_button_frame.setFrameShadow(QFrame.Raised)
        self.verticalLayout_9 = QVBoxLayout(self.slide_menu_button_frame)
        self.verticalLayout_9.setSpacing(0)
        self.verticalLayout_9.setObjectName(u"verticalLayout_9")
        self.verticalLayout_9.setContentsMargins(0, 0, 0, 0)

        self.horizontalLayout_3.addWidget(self.slide_menu_button_frame, 0, Qt.AlignLeft|Qt.AlignTop)

        self.empty_header_frame = QFrame(self.header_frame)
        self.empty_header_frame.setObjectName(u"empty_header_frame")
        self.empty_header_frame.setFrameShape(QFrame.StyledPanel)
        self.empty_header_frame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_5 = QHBoxLayout(self.empty_header_frame)
        self.horizontalLayout_5.setSpacing(0)
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.horizontalLayout_5.setContentsMargins(0, 0, 0, 0)

        self.horizontalLayout_3.addWidget(self.empty_header_frame)

        self.close_buttons_frame = QFrame(self.header_frame)
        self.close_buttons_frame.setObjectName(u"close_buttons_frame")
        self.close_buttons_frame.setStyleSheet(u"QPushButton{\n"
"	border:0px\n"
"}")
        self.close_buttons_frame.setFrameShape(QFrame.StyledPanel)
        self.close_buttons_frame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_4 = QHBoxLayout(self.close_buttons_frame)
        self.horizontalLayout_4.setSpacing(6)
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.horizontalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.minimize_button = QPushButton(self.close_buttons_frame)
        self.minimize_button.setObjectName(u"minimize_button")
        self.minimize_button.setStyleSheet(u"QPushButton:hover{\n"
"	icon: url(:/icons_magenta/icons/magenta/minimize-2.svg)\n"
"}")
        icon = QIcon()
        icon.addFile(u":/icons_black/icons/black/minimize-2.svg", QSize(), QIcon.Normal, QIcon.Off)
        self.minimize_button.setIcon(icon)
        self.minimize_button.setIconSize(QSize(24, 24))

        self.horizontalLayout_4.addWidget(self.minimize_button)

        self.maximize_button = QPushButton(self.close_buttons_frame)
        self.maximize_button.setObjectName(u"maximize_button")
        self.maximize_button.setStyleSheet(u"QPushButton:hover{\n"
"	icon: url(:/icons_magenta/icons/magenta/maximize.svg)\n"
"}\n"
"\n"
"QPushButton:checked{\n"
"	icon: url(:/icons_black/icons/black/minimize.svg)\n"
"}\n"
"\n"
"QPushButton:checked:hover{\n"
"	icon: url(:/icons_magenta/icons/magenta/minimize.svg)\n"
"}")
        icon1 = QIcon()
        icon1.addFile(u":/icons_black/icons/black/maximize.svg", QSize(), QIcon.Normal, QIcon.Off)
        self.maximize_button.setIcon(icon1)
        self.maximize_button.setIconSize(QSize(24, 24))
        self.maximize_button.setCheckable(True)

        self.horizontalLayout_4.addWidget(self.maximize_button)

        self.close_button = QPushButton(self.close_buttons_frame)
        self.close_button.setObjectName(u"close_button")
        self.close_button.setStyleSheet(u"QPushButton:hover{\n"
"	icon: url(:/icons_magenta/icons/magenta/x.svg)\n"
"}")
        icon2 = QIcon()
        icon2.addFile(u":/icons_black/icons/black/x.svg", QSize(), QIcon.Normal, QIcon.Off)
        self.close_button.setIcon(icon2)
        self.close_button.setIconSize(QSize(24, 24))

        self.horizontalLayout_4.addWidget(self.close_button)


        self.horizontalLayout_3.addWidget(self.close_buttons_frame, 0, Qt.AlignRight|Qt.AlignTop)


        self.verticalLayout_5.addWidget(self.header_frame, 0, Qt.AlignTop)

        self.main_body_frame = QFrame(self.main_body)
        self.main_body_frame.setObjectName(u"main_body_frame")
        sizePolicy1 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.main_body_frame.sizePolicy().hasHeightForWidth())
        self.main_body_frame.setSizePolicy(sizePolicy1)
        self.main_body_frame.setStyleSheet(u"QFrame{\n"
"	background-color: rgb(230, 0, 126);\n"
"}\n"
"\n"
"QFrame#login_frame, QFrame#frame_page_starten{\n"
"    border-radius: 15px;\n"
"	padding: 20 20 20 20;\n"
"}\n"
"\n"
"\n"
"QFrame#main_body_frame{\n"
"	background-color: white;\n"
"}\n"
"\n"
"QLineEdit, QCheckBox, QComboBox, QButton{\n"
"	min-height: 30px;\n"
"}\n"
"\n"
"QLabel{\n"
"	color: rgb(255, 255, 255);\n"
"	text-align:left;\n"
"    font: 800 35pt \"Anybody Thin ExtraBold\";\n"
"}\n"
"\n"
"QLabel#qrcode_label{\n"
"	background-color: rgb(255, 237, 0);\n"
"}\n"
"\n"
"\n"
"QPushButton{\n"
"    background-color: rgb(230, 0, 126);\n"
"    border: 2px solid white;\n"
"    border-radius: 4px;\n"
"    color: rgb(255, 255, 255);\n"
"	text-align:center;\n"
"	font: 500 18pt \"Anybody Thin Medium\";\n"
"	min-height: 30px;\n"
"	padding: 2 5 2 5;\n"
"	border-radius: 19px;\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"	background-color: rgb(255, 255, 255);\n"
"    color: rgb(230, 0, 126);\n"
"}\n"
"\n"
"QPushButton:!enabled{\n"
"    background-color: lightgray;\n"
""
                        "}\n"
"\n"
"QLineEdit{\n"
"	border-top-right-radius: 8px;\n"
"    border-top-left-radius: 8px;\n"
"	background-color: rgb(230, 0, 126);\n"
"	border-color: white;\n"
"	border-style: solid;\n"
"	border-width: 1px;\n"
"	border-top-style: none;\n"
"	border-right-style: none;\n"
"	border-left-style: none;\n"
"	color: rgb(255, 255, 255);\n"
"    font: 500 14pt \"Anybody Thin Medium\";\n"
"}\n"
"\n"
"QLineEdit::focus{\n"
"	background-color: rgb(207, 0, 113);\n"
"}")
        self.main_body_frame.setFrameShape(QFrame.StyledPanel)
        self.main_body_frame.setFrameShadow(QFrame.Raised)
        self.verticalLayout_10 = QVBoxLayout(self.main_body_frame)
        self.verticalLayout_10.setObjectName(u"verticalLayout_10")
        self.verticalLayout_10.setContentsMargins(0, 0, 0, 0)
        self.stackedWidget = QStackedWidget(self.main_body_frame)
        self.stackedWidget.setObjectName(u"stackedWidget")
        self.stackedWidget.setStyleSheet(u"")
        self.page_log = QWidget()
        self.page_log.setObjectName(u"page_log")
        self.verticalLayout = QVBoxLayout(self.page_log)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.protokoll_installieren = QTextEdit(self.page_log)
        self.protokoll_installieren.setObjectName(u"protokoll_installieren")
        self.protokoll_installieren.setEnabled(True)

        self.verticalLayout.addWidget(self.protokoll_installieren)

        self.button_ready = QPushButton(self.page_log)
        self.button_ready.setObjectName(u"button_ready")
        self.button_ready.setEnabled(False)
        self.button_ready.setCheckable(False)

        self.verticalLayout.addWidget(self.button_ready)

        self.stackedWidget.addWidget(self.page_log)
        self.page_starten = QWidget()
        self.page_starten.setObjectName(u"page_starten")
        self.horizontalLayout_2 = QHBoxLayout(self.page_starten)
        self.horizontalLayout_2.setSpacing(30)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(50, -1, -1, -1)
        self.frame_page_starten = QFrame(self.page_starten)
        self.frame_page_starten.setObjectName(u"frame_page_starten")
        sizePolicy2 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.frame_page_starten.sizePolicy().hasHeightForWidth())
        self.frame_page_starten.setSizePolicy(sizePolicy2)
        self.frame_page_starten.setMinimumSize(QSize(650, 450))
        self.verticalLayout_12 = QVBoxLayout(self.frame_page_starten)
        self.verticalLayout_12.setSpacing(29)
        self.verticalLayout_12.setObjectName(u"verticalLayout_12")
        self.verticalLayout_12.setContentsMargins(0, 0, 0, 0)
        self.stackedWidget_run = QStackedWidget(self.frame_page_starten)
        self.stackedWidget_run.setObjectName(u"stackedWidget_run")
        sizePolicy.setHeightForWidth(self.stackedWidget_run.sizePolicy().hasHeightForWidth())
        self.stackedWidget_run.setSizePolicy(sizePolicy)
        self.stackedWidget_run.setMinimumSize(QSize(0, 0))
        self.page_nicht_aktiv = QWidget()
        self.page_nicht_aktiv.setObjectName(u"page_nicht_aktiv")
        self.verticalLayout_7 = QVBoxLayout(self.page_nicht_aktiv)
        self.verticalLayout_7.setObjectName(u"verticalLayout_7")
        self.verticalLayout_7.setContentsMargins(0, 0, 0, 0)
        self.label_6 = RotatedLabel(self.page_nicht_aktiv)
        self.label_6.setObjectName(u"label_6")
        sizePolicy.setHeightForWidth(self.label_6.sizePolicy().hasHeightForWidth())
        self.label_6.setSizePolicy(sizePolicy)

        self.verticalLayout_7.addWidget(self.label_6)

        self.stackedWidget_run.addWidget(self.page_nicht_aktiv)
        self.page_aktiv = QWidget()
        self.page_aktiv.setObjectName(u"page_aktiv")
        self.verticalLayout_8 = QVBoxLayout(self.page_aktiv)
        self.verticalLayout_8.setObjectName(u"verticalLayout_8")
        self.verticalLayout_8.setContentsMargins(0, 0, 0, 0)
        self.frame = QFrame(self.page_aktiv)
        self.frame.setObjectName(u"frame")
        self.frame.setFrameShape(QFrame.NoFrame)
        self.frame.setFrameShadow(QFrame.Raised)
        self.gridLayout_2 = QGridLayout(self.frame)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.label_4 = QLabel(self.frame)
        self.label_4.setObjectName(u"label_4")

        self.gridLayout_2.addWidget(self.label_4, 4, 0, 1, 1)

        self.label_2 = QLabel(self.frame)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setStyleSheet(u"")

        self.gridLayout_2.addWidget(self.label_2, 2, 0, 1, 1)

        self.label_passwort = QLabel(self.frame)
        self.label_passwort.setObjectName(u"label_passwort")
        sizePolicy.setHeightForWidth(self.label_passwort.sizePolicy().hasHeightForWidth())
        self.label_passwort.setSizePolicy(sizePolicy)
        self.label_passwort.setMinimumSize(QSize(0, 34))
        self.label_passwort.setStyleSheet(u"	text-align:top left;\n"
"	font: 500 16pt \"Anybody Thin Medium\";")

        self.gridLayout_2.addWidget(self.label_passwort, 5, 0, 1, 1)

        self.label_webadresse = QPushButton(self.frame)
        self.label_webadresse.setObjectName(u"label_webadresse")
        self.label_webadresse.setMinimumSize(QSize(0, 34))
        font = QFont()
        font.setFamilies([u"Anybody Thin Medium"])
        font.setPointSize(16)
        font.setBold(False)
        font.setItalic(False)
        font.setUnderline(True)
        self.label_webadresse.setFont(font)
        self.label_webadresse.setStyleSheet(u"QPushButton{\n"
"    background-color: rgb(230, 0, 126);\n"
"    border: none;\n"
"    border-radius: 4px;\n"
"    color: rgb(255, 255, 255);\n"
"	text-align:top left;\n"
"	font: 500 16pt \"Anybody Thin Medium\";\n"
"	text-decoration: underline;\n"
"	min-height: 30px;\n"
"	padding: 2 5 2 5;\n"
"	border-radius: 19px;\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"    color: rgb(0, 0, 0);\n"
"}")

        self.gridLayout_2.addWidget(self.label_webadresse, 3, 0, 1, 1)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_2.addItem(self.verticalSpacer, 6, 0, 1, 1)


        self.verticalLayout_8.addWidget(self.frame)

        self.stackedWidget_run.addWidget(self.page_aktiv)

        self.verticalLayout_12.addWidget(self.stackedWidget_run)

        self.button_starten = QPushButton(self.frame_page_starten)
        self.button_starten.setObjectName(u"button_starten")
        self.button_starten.setCheckable(True)

        self.verticalLayout_12.addWidget(self.button_starten)


        self.horizontalLayout_2.addWidget(self.frame_page_starten, 0, Qt.AlignLeft|Qt.AlignVCenter)

        self.frame_2 = QFrame(self.page_starten)
        self.frame_2.setObjectName(u"frame_2")
        self.frame_2.setStyleSheet(u"background-color: rgb(255, 237, 0);")
        self.frame_2.setFrameShape(QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QFrame.Raised)
        self.verticalLayout_2 = QVBoxLayout(self.frame_2)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.qrcode_label = QLabel(self.frame_2)
        self.qrcode_label.setObjectName(u"qrcode_label")
        sizePolicy2.setHeightForWidth(self.qrcode_label.sizePolicy().hasHeightForWidth())
        self.qrcode_label.setSizePolicy(sizePolicy2)
        self.qrcode_label.setMinimumSize(QSize(325, 325))
        self.qrcode_label.setFrameShape(QFrame.NoFrame)

        self.verticalLayout_2.addWidget(self.qrcode_label)

        self.copy_qr_code_button = QPushButton(self.frame_2)
        self.copy_qr_code_button.setObjectName(u"copy_qr_code_button")
        sizePolicy2.setHeightForWidth(self.copy_qr_code_button.sizePolicy().hasHeightForWidth())
        self.copy_qr_code_button.setSizePolicy(sizePolicy2)
        self.copy_qr_code_button.setMinimumSize(QSize(325, 38))
        self.copy_qr_code_button.setStyleSheet(u"QPushButton{\n"
"    background-color: rgb(255, 237, 0);\n"
"    border: 2px solid rgb(230, 0, 126);\n"
"    border-radius: 4px;\n"
"    color: rgb(230, 0, 126);\n"
"	text-align:center;\n"
"	font: 500 18pt \"Anybody Thin Medium\";\n"
"	min-height: 30px;\n"
"	padding: 2 5 2 5;\n"
"	border-radius: 19px;\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"	background-color: rgb(230, 0, 126);\n"
"    color: rgb(255, 255, 255);\n"
"}")

        self.verticalLayout_2.addWidget(self.copy_qr_code_button)


        self.horizontalLayout_2.addWidget(self.frame_2, 0, Qt.AlignLeft|Qt.AlignVCenter)

        self.stackedWidget.addWidget(self.page_starten)
        self.page_installieren = QWidget()
        self.page_installieren.setObjectName(u"page_installieren")
        self.gridLayout_3 = QGridLayout(self.page_installieren)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.verticalSpacer_6 = QSpacerItem(20, 120, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_3.addItem(self.verticalSpacer_6, 0, 1, 1, 1)

        self.horizontalSpacer_5 = QSpacerItem(166, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_3.addItem(self.horizontalSpacer_5, 1, 0, 1, 1)

        self.frame_3 = QFrame(self.page_installieren)
        self.frame_3.setObjectName(u"frame_3")
        self.frame_3.setFrameShape(QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QFrame.Raised)
        self.verticalLayout_4 = QVBoxLayout(self.frame_3)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.label = QLabel(self.frame_3)
        self.label.setObjectName(u"label")

        self.verticalLayout_4.addWidget(self.label)

        self.button_installieren = QPushButton(self.frame_3)
        self.button_installieren.setObjectName(u"button_installieren")

        self.verticalLayout_4.addWidget(self.button_installieren)


        self.gridLayout_3.addWidget(self.frame_3, 1, 1, 1, 1)

        self.horizontalSpacer_6 = QSpacerItem(166, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_3.addItem(self.horizontalSpacer_6, 1, 2, 1, 1)

        self.verticalSpacer_5 = QSpacerItem(20, 119, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_3.addItem(self.verticalSpacer_5, 2, 1, 1, 1)

        self.stackedWidget.addWidget(self.page_installieren)
        self.page_login = QWidget()
        self.page_login.setObjectName(u"page_login")
        self.page_login.setLayoutDirection(Qt.LeftToRight)
        self.gridLayout_4 = QGridLayout(self.page_login)
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.gridLayout_4.setContentsMargins(50, -1, -1, -1)
        self.login_frame = QFrame(self.page_login)
        self.login_frame.setObjectName(u"login_frame")
        sizePolicy3 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.login_frame.sizePolicy().hasHeightForWidth())
        self.login_frame.setSizePolicy(sizePolicy3)
        self.login_frame.setMinimumSize(QSize(650, 350))
        self.login_frame.setStyleSheet(u"")
        self.login_frame.setFrameShape(QFrame.StyledPanel)
        self.login_frame.setFrameShadow(QFrame.Raised)
        self.verticalLayout_3 = QVBoxLayout(self.login_frame)
        self.verticalLayout_3.setSpacing(13)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.verticalLayout_3.setContentsMargins(9, -1, -1, -1)
        self.label_wsl_anmeldung = RotatedLabel(self.login_frame)
        self.label_wsl_anmeldung.setObjectName(u"label_wsl_anmeldung")
        self.label_wsl_anmeldung.setMinimumSize(QSize(0, 70))
        self.label_wsl_anmeldung.setWordWrap(True)

        self.verticalLayout_3.addWidget(self.label_wsl_anmeldung)

        self.lineEdit_username = QLineEdit(self.login_frame)
        self.lineEdit_username.setObjectName(u"lineEdit_username")
        self.lineEdit_username.setMinimumSize(QSize(300, 31))
        self.lineEdit_username.setAlignment(Qt.AlignBottom|Qt.AlignLeading|Qt.AlignLeft)

        self.verticalLayout_3.addWidget(self.lineEdit_username)

        self.lineEdit_password = QLineEdit(self.login_frame)
        self.lineEdit_password.setObjectName(u"lineEdit_password")
        self.lineEdit_password.setMinimumSize(QSize(300, 31))
        self.lineEdit_password.setEchoMode(QLineEdit.Password)
        self.lineEdit_password.setAlignment(Qt.AlignBottom|Qt.AlignLeading|Qt.AlignLeft)

        self.verticalLayout_3.addWidget(self.lineEdit_password)

        self.button_login = QPushButton(self.login_frame)
        self.button_login.setObjectName(u"button_login")
        sizePolicy4 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        sizePolicy4.setHorizontalStretch(0)
        sizePolicy4.setVerticalStretch(0)
        sizePolicy4.setHeightForWidth(self.button_login.sizePolicy().hasHeightForWidth())
        self.button_login.setSizePolicy(sizePolicy4)

        self.verticalLayout_3.addWidget(self.button_login)


        self.gridLayout_4.addWidget(self.login_frame, 0, 0, 1, 1, Qt.AlignLeft|Qt.AlignVCenter)

        self.label_7 = QLabel(self.page_login)
        self.label_7.setObjectName(u"label_7")
        self.label_7.setStyleSheet(u"background-color: rgb(255, 237, 0);")
        self.label_7.setPixmap(QPixmap(u":/images/logo_julis_nrw.png"))

        self.gridLayout_4.addWidget(self.label_7, 0, 1, 1, 1, Qt.AlignRight|Qt.AlignTop)

        self.stackedWidget.addWidget(self.page_login)
        self.page_install_wsl = QWidget()
        self.page_install_wsl.setObjectName(u"page_install_wsl")
        self.gridLayout_5 = QGridLayout(self.page_install_wsl)
        self.gridLayout_5.setObjectName(u"gridLayout_5")
        self.gridLayout_5.setContentsMargins(0, 0, 0, 0)
        self.verticalSpacer_7 = QSpacerItem(20, 124, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_5.addItem(self.verticalSpacer_7, 0, 1, 1, 1)

        self.horizontalSpacer_7 = QSpacerItem(186, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_5.addItem(self.horizontalSpacer_7, 1, 0, 1, 1)

        self.frame_4 = QFrame(self.page_install_wsl)
        self.frame_4.setObjectName(u"frame_4")
        self.frame_4.setFrameShape(QFrame.StyledPanel)
        self.frame_4.setFrameShadow(QFrame.Raised)
        self.verticalLayout_6 = QVBoxLayout(self.frame_4)
        self.verticalLayout_6.setObjectName(u"verticalLayout_6")
        self.label_3 = QLabel(self.frame_4)
        self.label_3.setObjectName(u"label_3")

        self.verticalLayout_6.addWidget(self.label_3)

        self.label_5 = QLabel(self.frame_4)
        self.label_5.setObjectName(u"label_5")

        self.verticalLayout_6.addWidget(self.label_5)


        self.gridLayout_5.addWidget(self.frame_4, 1, 1, 1, 1)

        self.horizontalSpacer_8 = QSpacerItem(185, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_5.addItem(self.horizontalSpacer_8, 1, 2, 1, 1)

        self.verticalSpacer_8 = QSpacerItem(20, 123, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_5.addItem(self.verticalSpacer_8, 2, 1, 1, 1)

        self.stackedWidget.addWidget(self.page_install_wsl)

        self.verticalLayout_10.addWidget(self.stackedWidget)


        self.verticalLayout_5.addWidget(self.main_body_frame)

        self.footer = QFrame(self.main_body)
        self.footer.setObjectName(u"footer")
        self.footer.setFrameShape(QFrame.StyledPanel)
        self.footer.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_7 = QHBoxLayout(self.footer)
        self.horizontalLayout_7.setSpacing(0)
        self.horizontalLayout_7.setObjectName(u"horizontalLayout_7")
        self.horizontalLayout_7.setContentsMargins(0, 0, 0, 0)
        self.footer_label_frame = QFrame(self.footer)
        self.footer_label_frame.setObjectName(u"footer_label_frame")
        self.footer_label_frame.setFrameShape(QFrame.StyledPanel)
        self.footer_label_frame.setFrameShadow(QFrame.Raised)
        self.verticalLayout_65 = QVBoxLayout(self.footer_label_frame)
        self.verticalLayout_65.setSpacing(0)
        self.verticalLayout_65.setObjectName(u"verticalLayout_65")
        self.verticalLayout_65.setContentsMargins(0, 0, 0, 0)
        self.footer_label = QLabel(self.footer_label_frame)
        self.footer_label.setObjectName(u"footer_label")

        self.verticalLayout_65.addWidget(self.footer_label)


        self.horizontalLayout_7.addWidget(self.footer_label_frame, 0, Qt.AlignBottom)

        self.empty_footer_frame = QFrame(self.footer)
        self.empty_footer_frame.setObjectName(u"empty_footer_frame")
        self.empty_footer_frame.setFrameShape(QFrame.StyledPanel)
        self.empty_footer_frame.setFrameShadow(QFrame.Raised)

        self.horizontalLayout_7.addWidget(self.empty_footer_frame)

        self.size_grip = QFrame(self.footer)
        self.size_grip.setObjectName(u"size_grip")
        self.size_grip.setMinimumSize(QSize(10, 10))
        self.size_grip.setMaximumSize(QSize(10, 10))
        self.size_grip.setFrameShape(QFrame.StyledPanel)
        self.size_grip.setFrameShadow(QFrame.Raised)

        self.horizontalLayout_7.addWidget(self.size_grip, 0, Qt.AlignRight|Qt.AlignBottom)


        self.verticalLayout_5.addWidget(self.footer, 0, Qt.AlignBottom)


        self.horizontalLayout.addWidget(self.main_body)

        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)

        self.stackedWidget.setCurrentIndex(1)
        self.stackedWidget_run.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.minimize_button.setText("")
        self.maximize_button.setText("")
        self.close_button.setText("")
        self.button_ready.setText(QCoreApplication.translate("MainWindow", u"Fertig", None))
        self.label_6.setText(QCoreApplication.translate("MainWindow", u"OpenSlides inaktiv.", None))
        self.label_4.setText(QCoreApplication.translate("MainWindow", u"Endpoint IP", None))
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"Webadresse", None))
        self.label_passwort.setText(QCoreApplication.translate("MainWindow", u"N/A", None))
        self.label_webadresse.setText(QCoreApplication.translate("MainWindow", u"N/A", None))
        self.button_starten.setText(QCoreApplication.translate("MainWindow", u"Start", None))
        self.qrcode_label.setText("")
        self.copy_qr_code_button.setText(QCoreApplication.translate("MainWindow", u"Kopieren", None))
        self.label.setText(QCoreApplication.translate("MainWindow", u"Openslides ist noch nicht installiert!", None))
        self.button_installieren.setText(QCoreApplication.translate("MainWindow", u"Installieren", None))
        self.label_wsl_anmeldung.setText(QCoreApplication.translate("MainWindow", u"Linux-Anmeldung", None))
        self.lineEdit_username.setInputMask("")
        self.lineEdit_username.setText("")
        self.lineEdit_username.setPlaceholderText(QCoreApplication.translate("MainWindow", u"Nutzername", None))
        self.lineEdit_password.setInputMask("")
        self.lineEdit_password.setText("")
        self.lineEdit_password.setPlaceholderText(QCoreApplication.translate("MainWindow", u"Passwort", None))
        self.button_login.setText(QCoreApplication.translate("MainWindow", u"Login", None))
        self.label_7.setText("")
        self.label_3.setText(QCoreApplication.translate("MainWindow", u"WSL nicht korrekt installiert!", None))
        self.label_5.setText(QCoreApplication.translate("MainWindow", u"Bitte installieren Sie WSL!", None))
        self.footer_label.setText(QCoreApplication.translate("MainWindow", u"Local Openslides - Erik Pr\u00fcmer", None))
    # retranslateUi

